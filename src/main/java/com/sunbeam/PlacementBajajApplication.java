package com.sunbeam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlacementBajajApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlacementBajajApplication.class, args);
	}

}
