package com.sunbeam.entity;

import java.util.Arrays;


public class Response {
	private boolean isSuccess;
	private String userId;
	private String emailId;
	private String rollNo;
	private Integer[] numbers;
	private String[] alphabets;

	public Response(boolean isSuccess, String userId, String emailId, String rollNo, Integer[] numbers,
			String[] alphabets) {
		super();
		this.isSuccess = isSuccess;
		this.userId = userId;
		this.emailId = emailId;
		this.rollNo = rollNo;
		this.numbers = numbers;
		this.alphabets = alphabets;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRollNo() {
		return rollNo;
	}

	public void setRollNo(String rollNo) {
		this.rollNo = rollNo;
	}

	public Integer[] getNumbers() {
		return numbers;
	}

	public void setNumbers(Integer[] numbers) {
		this.numbers = numbers;
	}

	public String[] getAlphabets() {
		return alphabets;
	}

	public void setAlphabets(String[] alphabets) {
		this.alphabets = alphabets;
	}

	@Override
	public String toString() {
		return "Response [isSuccess=" + isSuccess + ", userId=" + userId + ", emailId=" + emailId + ", rollNo=" + rollNo
				+ ", numbers=" + Arrays.toString(numbers) + ", alphabets=" + Arrays.toString(alphabets) + "]";
	}

}
